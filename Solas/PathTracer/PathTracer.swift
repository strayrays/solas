//
//  PathTracer.swift
//  Solas
//
//  Created by Jeff Porter on 8/16/19.
//  Copyright © 2019 Jeff Porter. All rights reserved.
//

import UIKit
import Geomx
import simd

class PathTracer {
	var width = 400
	var height = 400
    var image = [Color]()
	let camera = Camera()
	let numberOfSamples = 100

	private let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
	private let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue)

    var scene: [Intersectable] = [
        Sphere(center: SIMD3<Double>(0, 0, -1), radius: 0.5),
        Sphere(center: SIMD3<Double>(0, -100.5, -1), radius: 100)
    ]
    
    func renderTestImage() {
        image = [Color]()
        
        let testData = "rgbywk"
        for code in testData {
            switch code {
            case "r":
                image.append(Color(255, 0,  0))
            case "g":
                image.append(Color(0, 255, 0))
            case "b":
                image.append(Color(0, 0, 255))
            case "y":
                image.append(Color(255, 255, 0))
            case "w":
                image.append(Color(255, 255, 255))
            default:
                image.append(Color(0, 0, 0))
            }
        }
    }
    
    func gradient(ray: Ray) -> Color {
        let direction = normalize(ray.direction)
        let t = (direction.y + 1.0) / 2.0
        let gradient = (1.0 - t) * SIMD3<Double>([1, 1, 1]) + t * SIMD3<Double>([0.5, 0.7, 1.0])
        return Color(gradient)
    }

    func color(ray: Ray) -> Color {
        var closest: Hit? = nil
        
        for shape in scene {
            guard let hit = shape.intersect(with: ray, min: 0, max: Double.infinity) else {
                continue
            }
            
            switch closest {
            case .some(let currentClosest):
                if hit.t < currentClosest.t {
                    closest = hit
                }
            default:
                closest = hit
            }
        }
        
        guard let hit = closest else {
            return gradient(ray: ray)
        }
        
        return Color(x: (hit.normal.x + 1.0) / 2.0,
                     y: (hit.normal.y + 1.0) / 2.0,
                     z: (hit.normal.z + 1.0) / 2.0)
    }

	func imageFromBitmap(width: Int, height: Int, pixels: [Color]) -> UIImage? {
		let bitsPerComponent = 8
		let bitsPerPixel = 32
		let pixelSize = MemoryLayout.size(ofValue: Color())
		let bytesPerRow = pixelSize * width
		var data = pixels
		
		guard let provider = CGDataProvider(data: NSData(bytes: &data, length: data.count * pixelSize)) else {
			return nil
		}
		
		guard let cgImage = CGImage(width: width, height: height, bitsPerComponent: bitsPerComponent, bitsPerPixel: bitsPerPixel,
										bytesPerRow: bytesPerRow, space: rgbColorSpace, bitmapInfo: bitmapInfo, provider: provider,
										decode: nil, shouldInterpolate: true, intent: .defaultIntent) else {
			return nil
		}
		
		return UIImage(cgImage: cgImage)
	}
	
	func render(completion: @escaping (UIImage) -> Void) {
        image = [Color]()
        
        for y in (0 ..< height).reversed() {
            for x in 0 ..< width {
                // let index = y*width + x
                
				var pixel = Pixel()
				for _ in 0 ..< numberOfSamples {
					let u = Double(x) / Double(width)
					let v = Double(y) / Double(height)
					
					let ray = camera.ray(u: u, v: v)
					pixel = pixel + color(ray: ray)
				}
				
                image.append(Color(pixel / numberOfSamples))
            }
        }
		
		// Create a UIImage from the rendered pixels
		if let image = imageFromBitmap(width: width, height: height, pixels: image) {
			DispatchQueue.main.async {
				completion(image)
			}
		}
	}
}
