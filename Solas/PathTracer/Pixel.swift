//
//  Color.swift
//  Solas
//
//  Created by Jeff Porter on 8/19/19.
//  Copyright © 2019 Jeff Porter. All rights reserved.
//

import Foundation

struct Color {
	var alpha: UInt8 = 255
	var red: UInt8
	var green: UInt8
	var blue: UInt8
	
	init() {
		self.red = 0
		self.green = 0
		self.blue = 0
	}

    init(_ red: UInt8, _ green: UInt8, _ blue: UInt8) {
        self.red = red
        self.green = green
        self.blue = blue
    }
	
	init(x: Double, y: Double, z: Double) {
        self.init(UInt8(x*255.9), UInt8(y*255.9), UInt8(z*255.9))
    }
	
    init(_ vec: SIMD3<Double>) {
        self.init(x: vec.x, y: vec.y, z: vec.z)
    }
	
	init(_ pixel: Pixel) {
		self.alpha = UInt8(pixel.alpha)
		self.red = UInt8(pixel.red)
		self.green = UInt8(pixel.green)
		self.blue = UInt8(pixel.blue)
	}
}

struct Pixel {
	var alpha: UInt16 = 255
	var red: UInt16
	var green: UInt16
	var blue: UInt16
	
	init() {
		self.red = 0
		self.green = 0
		self.blue = 0
	}
	
    init(_ red: UInt16, _ green: UInt16, _ blue: UInt16) {
        self.red = UInt16(red)
        self.green = UInt16(green)
        self.blue = UInt16(blue)
    }

	init(red: UInt8, green: UInt8, blue: UInt8) {
		self.red = UInt16(red)
		self.green = UInt16(green)
		self.blue = UInt16(blue)
	}
	
	init(x: Double, y: Double, z: Double) {
        self.init(UInt16(x*255.9), UInt16(y*255.9), UInt16(z*255.9))
    }
	
    init(_ vec: SIMD3<Double>) {
        self.init(x: vec.x, y: vec.y, z: vec.z)
    }
	
	static func + (_ lhs: Pixel, _ rhs: Color) -> Pixel {
		Pixel(lhs.red + UInt16(rhs.red), lhs.green + UInt16(rhs.green), lhs.blue + UInt16(rhs.blue))
	}
	
	static func / (_ lhs: Pixel, _ rhs: Int) -> Pixel {
		Pixel(lhs.red/UInt16(rhs), lhs.green/UInt16(rhs), lhs.blue/UInt16(rhs))
	}
}
