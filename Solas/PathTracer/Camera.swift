//
//  Camera.swift
//  Solas
//
//  Created by Jeff Porter on 8/21/19.
//  Copyright © 2019 Jeff Porter. All rights reserved.
//

import Geomx

struct Camera {
	let origin = SIMD3<Double>([0, 0, 0])
	let lowerLeft = SIMD3<Double>([-2, -1, -1])
	let vertical = SIMD3<Double>([0, 4, 0])
	let horizontal = SIMD3<Double>([4, 0, 0])

	func ray(u: Double, v: Double) -> Ray {
		Ray(a: origin, b: lowerLeft + u*horizontal + v*vertical)
	}
}
