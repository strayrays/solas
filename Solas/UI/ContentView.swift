//
//  ContentView.swift
//  Solas
//
//  Created by Jeff Porter on 8/16/19.
//  Copyright © 2019 Jeff Porter. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
		VStack {
			Text("Solas")

			if UIApplication.shared.statusBarOrientation.isLandscape {
				VStack {
					HStack {
						RenderView()
							.frame(minWidth: nil, idealWidth: nil, maxWidth: .infinity, minHeight: nil, idealHeight: nil, maxHeight: .infinity, alignment: .top)
						.padding()
									
						Text("Render Info")
							.frame(minWidth: nil, idealWidth: nil, maxWidth: .infinity, minHeight: 100, idealHeight: 100, maxHeight: 400, alignment: .top)
					}
				}
			} else {
				VStack {
					RenderView()
						.frame(minWidth: nil, idealWidth: nil, maxWidth: .infinity, minHeight: nil, idealHeight: nil, maxHeight: .infinity, alignment: .top)
					.padding()
								
					Text("Render Info")
						.frame(minWidth: nil, idealWidth: nil, maxWidth: .infinity, minHeight: 100, idealHeight: 100, maxHeight: 400, alignment: .top)
				}
			}
		}
	}
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
