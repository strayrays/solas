//
//  RenderView.swift
//  Solas
//
//  Created by Jeff Porter on 8/16/19.
//  Copyright © 2019 Jeff Porter. All rights reserved.
//

import SwiftUI
import Geomx

struct RenderView: View {
	let pathTracer = PathTracer()
	@State var image = UIImage(systemName: "cube")!
	
	init() {

	}
	
	func render() {
		pathTracer.render { image in
			self.image = image
		}
	}
	
	var body: some View {
		VStack {
			Image(uiImage: self.image)
			.resizable(capInsets: EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0),
					   resizingMode: .stretch)
			.scaledToFill()
			.cornerRadius(10)
			.padding(EdgeInsets(top: 50, leading: 0, bottom: 8, trailing: 0))

			Button(action: render) {
				Image(systemName: "goforward")
			}
		}
	}
}

#if DEBUG
struct RenderView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
