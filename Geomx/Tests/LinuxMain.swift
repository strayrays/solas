import XCTest

import GeomxTests

var tests = [XCTestCaseEntry]()
tests += GeomxTests.allTests()
XCTMain(tests)
