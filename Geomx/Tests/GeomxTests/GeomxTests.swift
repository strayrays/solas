import XCTest
@testable import Geomx

final class GeomxTests: XCTestCase {
    func testVectorAdd() {
		let vector1 = Vector(SIMD3<Float>(1, 0, 0))
		let vector2 = Vector(SIMD3<Float>(0, 0, 1))
		let expectedResult = Vector(SIMD3<Float>(1, 0, 1))

        XCTAssertEqual(vector1 + vector2, expectedResult)
    }

    static var allTests = [
        ("testVectorAdd", testVectorAdd),
    ]
}
