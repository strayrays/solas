//
//  File.swift
//  
//
//  Created by Jeff Porter on 8/19/19.
//

import simd

public struct Ray {
    public let a: SIMD3<Double>
    public let b: SIMD3<Double>
    
    public var origin: SIMD3<Double> {
        get {
            return a
        }
    }
    
    public var direction: SIMD3<Double> {
        get {
            return b
        }
    }
    
	public init(a: SIMD3<Double>, b: SIMD3<Double>) {
		self.a = a
		self.b = b
	}
	
    public func pointAt(t: Double)  -> SIMD3<Double> {
        return a + t*b
    }
}
