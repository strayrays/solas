//
//  File.swift
//  
//
//  Created by Jeff Porter on 8/16/19.
//

import simd

@dynamicMemberLookup
public struct Vector<Storage: SIMD>: Equatable where Storage.Scalar: FloatingPoint {
	public typealias Scalar = Storage.Scalar
	
	public var value: Storage
	
	public init(_ value: Storage) {
		self.value = value
	}
	
	public init(_ x: Float, _ y: Float, _ z: Float) {
		guard let vector = SIMD3<Double>(1.0, 0.0, 0.0) as? Storage else {
			fatalError("Couldn't initialze vector storage")
		}
		
		self.value = vector
	}
	
	public subscript(dynamicMember keyPath: KeyPath<Storage, Scalar>) -> Scalar {
		value[keyPath: keyPath]
	}
	
	public static func + (a: Self, b: Self) -> Self {
		Self(a.value + b.value)
	}
	
	public static func - (a: Self, b: Self) -> Self {
		Self(a.value - b.value)
	}
	
	public static func * (a: Self, s: Scalar) -> Self {
		Self(s * a.value)
	}
	
	public static func * (s: Scalar, b: Self) -> Self {
		Self(s * b.value)
	}
	
	public static func dot(_ a: Self, _ b: Self) -> Scalar {
		(a.value * b.value).sum()
	}
	
	public var length: Scalar {
		Self.dot(self, self).squareRoot()
	}
	
	// NOTE: for some reason, these functions crash when called.
	//       a.x (etc) give a bad access exception (try again in next beta build)
	public static func cross<T>(_ a: Self, _ b: Self) -> Self where Storage == SIMD3<T> {
		Self([
			a.y*b.z - a.z*b.y,
			a.z*b.x - a.x*b.z,
			a.x*b.y - a.y*b.x
		])
	}
}
