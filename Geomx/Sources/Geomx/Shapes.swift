//
//  File.swift
//  
//
//  Created by Jeff Porter on 8/19/19.
//

import simd

public struct Hit {
    public let t: Double
    public let position: SIMD3<Double>
    public let normal: SIMD3<Double>
}

public protocol Intersectable {
    func intersect(with ray: Ray, min: Double, max: Double) -> Hit?
}

public struct Sphere: Intersectable {
	public let center: SIMD3<Double>
    public let radius: Double
    
	public init(center: SIMD3<Double>, radius: Double) {
		self.center = center
		self.radius = radius
	}
	
    public func intersect(with ray: Ray, min: Double, max: Double) -> Hit? {
        let oc = ray.origin - center
        let a = dot(ray.direction, ray.direction)
        let b = 2.0 * dot(oc, ray.direction)
        let c = dot(oc, oc) - radius * radius
        let discriminant = b*b - 4*a*c
        
        if discriminant > 0 {
            // FIXME: DRY this up
            var temp = (-b - sqrt(b*b - a*c)) / a
            if temp > min && temp < max {
                let t = temp
                let position = ray.pointAt(t: t)
                let normal = normalize((position - center) / radius)
                return Hit(t: t, position: position, normal: normal)
            }
            
            temp = (-b + sqrt(b*b - a*c)) / a
            if (temp > min && temp < max) {
                let t = temp
                let position = ray.pointAt(t: t)
                let normal = normalize((position - center) / radius)
                return Hit(t: t, position: position, normal: normal)
            }
        }
        
        return nil
    }
}
